#/usr/bin/bash

# Scenario 2 - Run docker and do smoke test
./mvnw clean  package 
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
docker build -t springio/gs-spring-boot-docker .
docker run -dp 8090:8090 -t springio/gs-spring-boot-docker
curl -s localhost:8090/; echo
docker rm $(docker ps -aq)
docker rmi springio/gs-spring-boot-docker:latest
