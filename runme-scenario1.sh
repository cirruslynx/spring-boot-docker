#/usr/bin/bash

# Scenario 1 - Run outside of docker and do smoke test
./mvnw clean  package && (java -jar target/spring-boot-docker-0.0.1-SNAPSHOT.jar ) &
sleep 3
curl -s localhost:8090/; echo
kill % 1