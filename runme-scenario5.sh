#/usr/bin/bash

# Scenario 5- Build docker with maven and  set up remote debug 
./mvnw clean  package 
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
docker build -t springio/gs-spring-boot-docker .
docker run -e "JAVA_TOOL_OPTIONS=-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n" -p 8090:8090 -p 5005:5005 -t springio/gs-spring-boot-docker
