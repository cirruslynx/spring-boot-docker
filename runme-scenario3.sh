#/usr/bin/bash

# Scenario 3- Build docker with maven and do smoke test
./mvnw spring-boot:build-image -Dspring-boot.build-image.imageName=springio/gs-spring-boot-docker
docker run -dp 8090:8090 -t springio/gs-spring-boot-docker
curl -s localhost:8090/; echo
