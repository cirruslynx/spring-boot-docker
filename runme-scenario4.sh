#/usr/bin/bash

# Scenario 4 - Build docker with maven pass in dev Spring Profile and do smoke test
./mvnw spring-boot:build-image -Dspring-boot.build-image.imageName=springio/gs-spring-boot
docker run -d -e "SPRING_PROFILES_ACTIVE=dev" -p 8090:8090 -t springio/gs-spring-boot-docker
curl -s localhost:8090/; echo
