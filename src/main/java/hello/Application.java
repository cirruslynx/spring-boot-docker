package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

	Logger logger = LoggerFactory.getLogger(Application.class);
	
	@Value("${messages.greeting}")
	String message;

	@RequestMapping("/")
	public String home() {
		logger.debug("Returning messages.greeting " + message);
		return message;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}